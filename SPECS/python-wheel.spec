# Note that the only function of bootstrap is that it disables the test suite:
#   bcond_with bootstrap = tests enabled
%bcond_with bootstrap

# Note(hguemar): EL7 has no python3 stack but EPEL does
%if 0%{?rhel} && 0%{?rhel} <= 7 && 0%{!?epel:1}
%define _without_python3 1
%endif

%bcond_with python2
%bcond_without python3

%global pypi_name wheel

Name:           python-%{pypi_name}
Version:        0.30.0
Release:        6%{?dist}
Epoch:          1
Summary:        Built-package format for Python

License:        MIT
URL:            https://github.com/pypa/wheel
Source0:        %{url}/archive/%{version}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

# Latest version of wheel requires the package keyrings.alt in order for the tests to pass,
# however it can't be packaged for Fedora as of yet since the code is not licensed,
# and as a result wheel fails to build from source.
# Review request of keyrings.alt: https://bugzilla.redhat.com/show_bug.cgi?id=1365794
# Until the license issue is resolved upstream, this patch is added to revert
# the commit from wheel, that introduced this dependency.
# https://bitbucket.org/pypa/wheel/commits/06841295888fdb430abe12aae29da92107e7360a
Patch0: remove-keyrings.alt-dependency.patch

%global _description \
A built-package format for Python.\
\
A wheel is a ZIP-format archive with a specially formatted filename and the\
.whl extension. It is designed to contain all the files for a PEP 376\
compatible install in a way that is very close to the on-disk format.

%description %{_description}

%if %{with python2}
%package -n     python2-%{pypi_name}
Summary:        %{summary}
BuildRequires:  python2-devel
BuildRequires:  python2-setuptools
%if ! %{with bootstrap}
BuildRequires:  python2-pytest
%endif
%{?python_provide:%python_provide python2-%{pypi_name}}

%description -n python2-%{pypi_name} %{_description}

Python 2 version.
%endif


%if %{with python3}
%package -n     python3-%{pypi_name}
Summary:        %{summary}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%if ! %{with bootstrap}
BuildRequires:  python3-pytest
%endif
%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name} %{_description}

Python 3 version.
%endif


%prep
%autosetup -n %{pypi_name}-%{version} -p1
# remove unneeded shebangs
sed -ie '1d' %{pypi_name}/{egg2wheel,wininst2wheel}.py

# Remove tests for optional functionality which depends on bad crypto
# (pycryptopp -> python-keyring -> python-wheel)
# See https://bugzilla.redhat.com/show_bug.cgi?id=1561576
rm tests/test_signatures.py
sed -i -e's/^def test_keygen():$/def skip_test_keygen():/' tests/test_tool.py


%build
%if %{with python2}
%py2_build
%endif
%if %{with python3}
%py3_build
%endif


%install
%if %{with python3}
%py3_install
mv %{buildroot}%{_bindir}/%{pypi_name}{,-%{python3_version}}
%endif

%if %{with python2}
%py2_install
mv %{buildroot}%{_bindir}/%{pypi_name}{,-%{python2_version}}
%endif

%if ! %{with bootstrap}
%check
rm setup.cfg

# Remove part of the test that uses the "jsonschema" package
sed -i '/jsonschema/d' tests/test_bdist_wheel.py

%if %{with python2}
PYTHONPATH=%{buildroot}%{python2_sitelib} py.test-2 -v --ignore build
%endif
%if %{with python3}
PYTHONPATH=%{buildroot}%{python3_sitelib} py.test-3 -v --ignore build
%endif
%endif

%if %{with python2}
%files -n python2-%{pypi_name}
%license LICENSE.txt
%doc CHANGES.txt README.rst
%{_bindir}/%{pypi_name}-%{python2_version}
%{python2_sitelib}/%{pypi_name}*
%endif

%if %{with python3}
%files -n python3-%{pypi_name}
%license LICENSE.txt
%doc CHANGES.txt README.rst
%{_bindir}/%{pypi_name}-%{python3_version}
%{python3_sitelib}/%{pypi_name}*
%endif

%changelog
* Mon Aug 06 2018 Petr Viktorin <pviktori@redhat.com> - 1:0.30.0-6
- Remove unversioned executables (only *-3.6 should be provided)

* Mon Jun 25 2018 Petr Viktorin <pviktori@redhat.com> - 1:0.30.0-5
- Don't build the python2 subpackage
  https://bugzilla.redhat.com/show_bug.cgi?id=1594353

* Tue Jun 12 2018 Petr Viktorin <pviktori@redhat.com> - 1:0.30.0-4
- Also remove test dependency on python3-jsonschema

* Wed May 30 2018 Petr Viktorin <pviktori@redhat.com> - 1:0.30.0-3
- Remove test dependency on python2-jsonschema
  https://bugzilla.redhat.com/show_bug.cgi?id=1584189

* Tue Apr 10 2018 Petr Viktorin <pviktori@redhat.com> - 1:0.30.0-2
- Remove build-time (test) dependency on python-keyring

* Fri Feb 23 2018 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 1:0.30.0-1
- Update to 0.30.0

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.30.0a0-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Tue Aug 29 2017 Tomas Orsava <torsava@redhat.com> - 0.30.0a0-8
- Switch macros to bcond's and make Python 2 optional to facilitate building
  the Python 2 and Python 3 modules

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.30.0a0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.30.0a0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Jan 03 2017 Charalampos Stratakis <cstratak@redhat.com> - 0.30.0a0-5
- Enable tests

* Fri Dec 09 2016 Charalampos Stratakis <cstratak@redhat.com> - 0.30.0a0-4
- Rebuild for Python 3.6 without tests

* Tue Dec 06 2016 Igor Gnatenko <i.gnatenko.brain@gmail.com> - 0.30.0a0-3
- Add bootstrap method

* Mon Sep 19 2016 Charalampos Stratakis <cstratak@redhat.com> - 0.30.0a0-2
- Use the python_provide macro

* Mon Sep 19 2016 Charalampos Stratakis <cstratak@redhat.com> - 0.30.0a0-1
- Update to 0.30.0a0
- Added patch to remove keyrings.alt dependency

* Wed Aug 10 2016 Igor Gnatenko <ignatenko@redhat.com> - 0.29.0-1
- Update to 0.29.0
- Cleanups and fixes

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.26.0-3
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.26.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Oct 13 2015 Robert Kuska <rkuska@redhat.com> - 0.26.0-1
- Update to 0.26.0
- Rebuilt for Python3.5 rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.24.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Tue Jan 13 2015 Slavek Kabrda <bkabrda@redhat.com> - 0.24.0-3
- Make spec buildable in EPEL 6, too.
- Remove additional sources added to upstream tarball.

* Sat Jan 03 2015 Matej Cepl <mcepl@redhat.com> - 0.24.0-2
- Make python3 conditional (switched off for RHEL-7; fixes #1131111).

* Mon Nov 10 2014 Slavek Kabrda <bkabrda@redhat.com> - 0.24.0-1
- Update to 0.24.0
- Remove patches merged upstream

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.22.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri Apr 25 2014 Matej Stuchlik <mstuchli@redhat.com> - 0.22.0-3
- Another rebuild with python 3.4

* Fri Apr 18 2014 Matej Stuchlik <mstuchli@redhat.com> - 0.22.0-2
- Rebuild with python 3.4

* Thu Nov 28 2013 Bohuslav Kabrda <bkabrda@redhat.com> - 0.22.0-1
- Initial package.
